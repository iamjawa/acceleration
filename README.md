Acceleration
===========

Acceleration is designed as a lightweight basic Physics calculator, aimed at secondary school students. I needed to speed up my Physics Homework, so I built this. 

'I choose a lazy person to do a hard job. Because a lazy person will find an easy way to do it.' - Bill Gates

Using the program is as easy as:

    Open acceleration.py
    Follow the on-screen instructions

Features
--------

- Calculate Rates of Acceleration
- Calculate "F" (Force of Gravity)
- Calculate Speed / Distance / Time

Installation
------------

Install Python by running:
	python.msi - Windows
	python.dmg - Mac OS X
if you do not already have Python installed on your PC or Mac.

Contribute
----------

- Source Code: github.com/iamjawa/acceleration

Support
-------

If you are having issues, please let us know.
You can open a ticket or E-Mail me at: hello@iamjawa.co

License
-------

The project is licensed under the WTFPL license which you can find in "license.txt".
