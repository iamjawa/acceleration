loop = 1
while loop == 1:
    print ("---------------------------------------------------------------------")
    print ("Welcome to Acceleration.")
    print ("1 = Acceleration Calculator")
    print ("2 = Force Of Gravity Calculator")
    print ("3 = Speed / Distance / Time Calculator")
    user_choice = int(input("Please choose your option: "))
    if user_choice == 1:
    	final_v = int(input("What was the final velocity of the object (in m/s)? "))
    	initial_v = int(input("What was the initial velocity of the object (in m/s)? "))
    	velocity_time = int(input("How long was the object travelling for (in seconds?) "))
    	fmini = final_v - initial_v
    	print ("The object was accelerating at a rate of", fmini / velocity_time,"m/s squared")
    	continue
    if user_choice == 2:
        g = 10
        m1 = int(input("What is the first mass (in kg)? "))
        m2 = int(input("What is the second mass (in kg)? "))
        r = int(input("What is the distance between the two masses (in metres)? "))
        first = m1 * m2
        second = g * first
        f = second / r ** 2
        print ("The gravitational force between the masses is", f,"N")
        continue
    if user_choice == 3:
        print ("1 = Calculate Speed")
        print ("2 = Calculate Distance")
        print ("3 = Calculate Time")
        spchoice = int(input("Please choose your option: "))
        if spchoice == 1:
            spdistance = int(input("How far did you travel (in miles)? "))
            sptime = int(input("How long did you travel for (in hours)? "))
            spans = spdistance / sptime
            print ("Your average speed was", spans,"mph")
            continue
        if spchoice == 2:
            dispeed = int(input("What was your average speed (in mph)? "))
            ditime  = int(input("How long did you travel for (in hours)? "))
            dians = dispeed * ditime
            print ("You travelled for", dians,"miles")
            continue
        if spchoice == 3:
            tidistance = int(input("How far did you travel (in miles)? "))
            tispeed = int(input("What was your average speed (in mph)? "))
            tians = tidistance / tispeed
            print ("You travelled for", tians,"hours")
            continue
        else:
            print ("Please enter a valid option!")
            continue
    else:
        print ("Please enter a valid option!")
        continue
    

    
